// Validate
$("form:not(.no_validate)").each(function(){
    var th = $(this);
    th.validate({
        rules: {
            fn: {
                required: true,
                minlength: 2,
            },
            f_Email: {
                required: true,
                email: true
            },
            org:"required",
            rcquestion: "required"
        },
        messages: {
            fn: {
                required: "Необходимое поле",
                minlength: "Мало букв"
            },
            f_Email: {
                required: "Необходимое поле ",
                email: "Введите правильный email !"
            },
            rcquestion: "Введите вопрос",
            org:"Введите организацию"
        },
        submitHandler: function(){
            //alert("работает");
            $('#rc-contactsform').addClass('success').html('<img src="./img/success_mail.png"/><div class="success_text">Сообщение  успешно отправлено</div>');

            // $.post(th.attr("action"), th.serialize(), function (response) {
            //  th.parent("div").html("<div class='form_response'>" + response + "</div>");
            // });
        }
    });
});

$('#rc-contactsform .input-wrap label').click(function(){
    $(this).remove();
});

$('#rc-contactsform .input-wrap input').focus(function(){
    $(this).removeClass('error').siblings('label').remove();
});